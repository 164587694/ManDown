package reone.android.mandown.Model;

public class ScreenAttribute {
	public int minX;
	public int minY;
	public int maxX;
	public int maxY;

	public ScreenAttribute(int minX, int minY, int maxX, int maxY) {
		this.maxX = maxX;
		this.maxY = maxY;
		this.minX = minX;
		this.minY = minY;
	}
}
