package reone.android.mandown.Model;


public class RankItem {
    public int no;
    public String name;
    public int score;
    public String date;

    @Override
    public String toString() {
        return "RankItem{" +
                "no=" + no +
                ", name='" + name + '\'' +
                ", score=" + score +
                ", date='" + date + '\'' +
                '}';
    }
}
