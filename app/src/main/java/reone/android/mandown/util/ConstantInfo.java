package reone.android.mandown.util;


public interface ConstantInfo {


    public static final String SHAREPREFERENCES_NAME = "reone.mandown.SharedPreferences";

	public static final String PREFERENCE_KEY_SOUNDS = "reone.android.mandown.sounds";
	public static final String PREFERENCE_KEY_VIBRATE = "reone.android.mandown.vibrate";
	public static final String PREFERENCE_KEY_SHOWTIPS = "reone.android.mandown.showtips";
	public static final String PREFERENCE_KEY_POWER = "reone.android.mandown.power";

	public static final String PREFERENCE_KEY_RANKING_NAME = "reone.android.mandown.ranking.name";
	public static final String PREFERENCE_KEY_RANKING_SCORE = "reone.android.mandown.ranking.score";
	public static final String PREFERENCE_KEY_RANKING_DATE = "reone.android.mandown.ranking.date";

    public static final String PREFERENCE_KEY_BACKGROUD_RES = "reone.android.mandown.backgroud.res";

}