package reone.android.mandown.util;

/**
 * @author wangxingsheng
 */
public class StringUtil {

    /**
     * 是否为空
     *
     * @param s
     * @return
     */
    public static boolean isEmpty(String s) {
        return s == null || "".equals(s.trim());
    }

    public static String toLongString(Object... str){
        StringBuffer sb = new StringBuffer();
        for(Object s:str){
            sb.append(s);
        }
        return sb.toString();
    }
}
