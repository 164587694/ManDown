package reone.android.mandown.util;

import android.util.Log;

import reone.android.mandown.BuildConfig;

public class LogUtil {
    public static boolean IS_DEBUG = true;
    public static final String TAG = "manDown";

    public static void v(String tag, String msg) {
        if (IS_DEBUG)
            Log.v(TAG, tag + "------" + msg);
    }

    public static void i(String tag, String msg) {
        if (IS_DEBUG)
            Log.i(TAG, tag + "------" + msg);
    }

    public static void d(String tag, String msg) {
        if (IS_DEBUG)
            Log.d(TAG, tag + "------" + msg);
    }

    public static void e(String tag, String msg) {
        if (IS_DEBUG)
            Log.e(TAG, tag + "------" + msg);
    }

    public static void w(String tag, String msg) {
        if (IS_DEBUG)
            Log.w(TAG, tag + "------" + msg);
    }

    public static void w(String tag, String msg, Throwable thr) {
        if (IS_DEBUG)
            Log.w(TAG, tag + "------" + msg, thr);
    }

    public static void ss(String msg) {
        if (IS_DEBUG)
            Log.v(TAG, msg);
    }
}
