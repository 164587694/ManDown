package reone.android.mandown.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Build;
import android.os.SystemClock;
import android.view.Display;
import android.view.WindowManager;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * @author wangxingsheng
 */
public class AppTools {
    public static Context mContext;

    public static int getRandomNumber(int from,int to){
        return (int)(Math.random()*(to-from+1));
    }

    public static int getScreenWidth() {
        int result = 0;
        WindowManager wm = (WindowManager) mContext.getSystemService(mContext.WINDOW_SERVICE);
        Display defaultDisplay = wm.getDefaultDisplay();
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR2) {
            result = defaultDisplay.getWidth();
        } else {
            Point outSize = new Point();
            defaultDisplay.getSize(outSize);
            result = outSize.x;
        }

        return result;
    }

    public static int getScreenHeight() {
        int result = 0;
        WindowManager wm = (WindowManager) mContext.getSystemService(mContext.WINDOW_SERVICE);
        Display defaultDisplay = wm.getDefaultDisplay();
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR2) {
            result = defaultDisplay.getHeight();
        } else {
            Point outSize = new Point();
            defaultDisplay.getSize(outSize);
            result = outSize.y;
        }

        return result;
    }
    /**
     * 根据手机的分辨率从 dp 的单位 转成为 px(像素)
     */
    public static int dip2px(float dpValue) {
        final float scale = mContext.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    /**
     * 根据手机的分辨率从 px(像素) 的单位 转成为 dp
     */
    public static int px2dip(float pxValue) {
        final float scale = mContext.getResources().getDisplayMetrics().density;
        return (int) (pxValue / scale + 0.5f);
    }

    public static long getCurrentTime(){
        return SystemClock.elapsedRealtime();
    }

    /**
     * 得到当前时间 MM月dd日 HH:mm
     *
     * @return
     */
    public static String getCurrentDate() {

        SimpleDateFormat dateFromat = new SimpleDateFormat("MM月dd日 HH:mm");

        dateFromat.setTimeZone(TimeZone.getTimeZone("GMT+8"));

        String str = null;
        try {
            str = dateFromat.format(new Date());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str;
    }

    public static SharedPreferences getSharedPreferences(){
        return mContext.getApplicationContext().getSharedPreferences(ConstantInfo.SHAREPREFERENCES_NAME,0);
    }
    public static SharedPreferences.Editor getSharedPreferencesEdit(){
        return mContext.getApplicationContext().getSharedPreferences(ConstantInfo.SHAREPREFERENCES_NAME,0).edit();
    }
}
