package reone.android.mandown.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import java.util.ArrayList;

import reone.android.mandown.Model.RankItem;
import reone.android.mandown.util.AppTools;
import reone.android.mandown.util.LogUtil;
import reone.android.mandown.util.StringUtil;

public class ScoreDBManager {

    SQLiteHelper openHelper;
    SQLiteDatabase db;

    public ScoreDBManager(Context context) {
        this.openHelper = new SQLiteHelper(context);
        this.db = this.openHelper.getWritableDatabase();
    }

    public boolean insert(String name, int score) {
        LogUtil.ss(StringUtil.toLongString("insert"," name = ",name," score =",score));
        if (name == null || score < 0)
            return false;
        db.beginTransaction();
        boolean ret = false;
        try {
            ContentValues values = new ContentValues();
            values.put(ScoreEntity.COLUMN_NAME_SCORE_NAME, name);
            values.put(ScoreEntity.COLUMN_NAME_SCORE_NUM, score);
            values.put(ScoreEntity.COLUMN_NAME_SCORE_DATE, AppTools.getCurrentDate());
            db.insertOrThrow(ScoreEntity.TABLE_NAME, ScoreEntity._ID, values);
            db.setTransactionSuccessful();
            LogUtil.ss("insert success");
            ret =true;
        } catch (Exception e){
            LogUtil.ss(e!=null?e.toString():"sql error");
            ret = false;
        }finally {
            db.endTransaction();
            db.close();
            openHelper.close();
        }
        return ret;
    }
    public ArrayList<RankItem> selectAll() {
        LogUtil.ss("selectAll");

        Cursor cursor = null;
        ArrayList<RankItem> list = new ArrayList<>();
        try {
            cursor = db.query(ScoreEntity.TABLE_NAME, null, null, null, null, null, ScoreEntity.COLUMN_NAME_SCORE_NUM+" DESC");

            if (cursor != null && cursor.getCount() > 0) {
                while (cursor.moveToNext()){
                    RankItem rank = new RankItem();
                    rank.no =  cursor.getInt(cursor.getColumnIndex(ScoreEntity._ID));
                    rank.name =  cursor.getString(cursor.getColumnIndex(ScoreEntity.COLUMN_NAME_SCORE_NAME));
                    rank.score =  cursor.getInt(cursor.getColumnIndex(ScoreEntity.COLUMN_NAME_SCORE_NUM));
                    rank.date =  cursor.getString(cursor.getColumnIndex(ScoreEntity.COLUMN_NAME_SCORE_DATE));
                    list.add(rank);
                    LogUtil.ss("get a rank = " + rank + " --> _ID = " + rank.no);
                }
            }

        } finally {
            if (cursor != null)
                cursor.close();

            db.close();
            openHelper.close();
        }
        return list;
    }

    public void toInsertHighscore(String name, int score){
        Cursor cursor = null;
        try {
            cursor = db.query(ScoreEntity.TABLE_NAME,
                    new String[]{ScoreEntity.COLUMN_NAME_SCORE_NUM},
                    StringUtil.toLongString(ScoreEntity.COLUMN_NAME_SCORE_NAME,"=?"),
                    new String[]{name},
                    null, null, ScoreEntity.COLUMN_NAME_SCORE_NUM+" DESC");
            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();
                int highScore = cursor.getInt(cursor.getColumnIndex(ScoreEntity.COLUMN_NAME_SCORE_NUM));
                LogUtil.ss(StringUtil.toLongString("toInsertHighscore name ",name," score = ",score," high = ",highScore));
                if(highScore < score){
                    insert(name, score);
                }
            }
        }finally {
            if (cursor != null)
                cursor.close();

            db.close();
            openHelper.close();
        }
    }

    public abstract class ScoreEntity implements BaseColumns {
        public static final String TABLE_NAME = "score";
        public static final String COLUMN_NAME_SCORE_NAME = "score_name";
        public static final String COLUMN_NAME_SCORE_NUM = "score_num";
        public static final String COLUMN_NAME_SCORE_DATE = "score_date";
    }
}
