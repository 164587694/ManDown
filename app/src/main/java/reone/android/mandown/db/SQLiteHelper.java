package reone.android.mandown.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import reone.android.mandown.util.LogUtil;

public class SQLiteHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "reone.mandown.db";
    private static final int DATABASE_VERSION = 7;
    private final String CREATE_TABLE = "CREATE TABLE ";
    private final String TEXT_TYPE = " TEXT";
    private final String INTEGER_TYPE = " INTEGER";
    private final String AUTOINCREMENT = " AUTOINCREMENT";
    private final String NOT_NULL = " NOT NULL";
    private final String PRIMARY_KEY = " PRIMARY KEY";
    private final String COMMA_SEP = ",";
    private final String LEFT_PARENTHESIS = " (";
    private final String RIGHT_PARENTHESIS = " )";
    private final String SQL_CREATE_MESSAGE_ENTITY = CREATE_TABLE + ScoreDBManager.ScoreEntity.TABLE_NAME
            + LEFT_PARENTHESIS
            + ScoreDBManager.ScoreEntity._ID + INTEGER_TYPE + PRIMARY_KEY + AUTOINCREMENT + COMMA_SEP
            + ScoreDBManager.ScoreEntity.COLUMN_NAME_SCORE_NAME + TEXT_TYPE + COMMA_SEP
            + ScoreDBManager.ScoreEntity.COLUMN_NAME_SCORE_DATE + INTEGER_TYPE + COMMA_SEP
            + ScoreDBManager.ScoreEntity.COLUMN_NAME_SCORE_NUM + INTEGER_TYPE
            + RIGHT_PARENTHESIS;

    private final String DROP_TABLE = "DROP TABLE IF EXISTS ";
    private final String SQL_DROP_MESSAGE_ENTITY = DROP_TABLE + ScoreDBManager.ScoreEntity.TABLE_NAME;

    public SQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_MESSAGE_ENTITY);
        LogUtil.ss("onCreate execSQL");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DROP_MESSAGE_ENTITY);
        onCreate(db);
        LogUtil.ss("onUpgrade execSQL");
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DROP_MESSAGE_ENTITY);
        onCreate(db);
        LogUtil.ss("onDowngrade execSQL");
    }
}
