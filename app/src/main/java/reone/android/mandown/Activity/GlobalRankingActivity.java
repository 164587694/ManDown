package reone.android.mandown.Activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import reone.android.mandown.Activity.sup.BaseActivity;
import reone.android.mandown.R;
import reone.android.mandown.Model.RankItem;
import reone.android.mandown.db.ScoreDBManager;
import reone.android.mandown.util.LogUtil;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * activity for global ranking
 * 
 */
public class GlobalRankingActivity extends BaseActivity {

	private static int HANDLE_MESSAGE_UI_UPDATE = 1;

	private ProgressDialog mProgressDialog;

    private ListView scores;
    private ArrayList<HashMap<String, String>> rankDatas = new ArrayList<>();
    private String[] dataKeys = new String[]{
            "no","name","score","date"
    };

    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ranking);
        scores = (ListView)findViewById(R.id.ranking_list);

		mProgressDialog = ProgressDialog.show(this, "", getResources()
				.getString(R.string.ranking_title_loading_info), true, true,
				new OnCancelListener() {
					@Override
					public void onCancel(DialogInterface arg0) {
						GlobalRankingActivity.this.finish();
					}
				});

		new RankingDataThread(new Handler() {
			public void handleMessage(Message msg) {
				super.handleMessage(msg);
				if (msg.what == GlobalRankingActivity.HANDLE_MESSAGE_UI_UPDATE) {
					initList();
                    scores.invalidate();
				}
			}
		}).start();
	}
    private void initList(){
        scores.setAdapter(new SimpleAdapter(this, rankDatas, R.layout.ranking_list_item,
                dataKeys,
                new int[]{
                        R.id.ranking_no,
                        R.id.ranking_name,
                        R.id.ranking_score,
                        R.id.ranking_date
                }));
    }

	class RankingDataThread extends Thread {

		private Handler mHandler;

		public RankingDataThread(Handler handler) {
			this.mHandler = handler;
		}

		public void run() {

            ArrayList<RankItem> list = getRankingInfo();
            int count = 0;
            for (RankItem rank:list){
                HashMap<String, String> map = new HashMap<>();
                map.put(dataKeys[0],++count +"");
                map.put(dataKeys[1],rank.name);
                map.put(dataKeys[2],rank.score+"");
                map.put(dataKeys[3],rank.date+"");
                rankDatas.add(map);
            }

			Message message = new Message();
			message.what = GlobalRankingActivity.HANDLE_MESSAGE_UI_UPDATE;
			mHandler.sendMessage(message);
			mProgressDialog.dismiss();
		}
	}

	public ArrayList<RankItem> getRankingInfo() {
        LogUtil.ss("getRankingInfo");
		return new ScoreDBManager(this).selectAll();
	}
}
