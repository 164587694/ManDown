package reone.android.mandown.Activity;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;

import reone.android.mandown.Activity.sup.BaseActivity;
import reone.android.mandown.View.AgileBuddyView;
import reone.android.mandown.R;

/**
 * main activity of AgileBuddy
 * 
 *
 */
public class AgileBuddyActivity extends BaseActivity {

	private AgileBuddyView mAgileBuddyView;

	private SensorManager mSensorManager;

	private Sensor mSensor;

	private SensorEventListener mSensorEventListener;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		mAgileBuddyView = (AgileBuddyView) findViewById(R.id.agile_buddy);
		mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
		mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);//加速传感器
		mSensorEventListener = new SensorEventListener() {
			public void onSensorChanged(SensorEvent e) {
				mAgileBuddyView.handleMoving(e.values[SensorManager.DATA_X]);
			}

			public void onAccuracyChanged(Sensor s, int accuracy) {
			}
		};
		mSensorManager.registerListener(mSensorEventListener, mSensor,
				SensorManager.SENSOR_DELAY_GAME);
	}

	@Override
	public void finish() {
		mSensorManager.unregisterListener(mSensorEventListener, mSensor);
		super.finish();
	}
}