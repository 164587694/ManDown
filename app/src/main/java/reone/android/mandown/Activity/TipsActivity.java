package reone.android.mandown.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import reone.android.mandown.Activity.sup.BaseActivity;
import reone.android.mandown.R;

public class TipsActivity extends BaseActivity implements OnClickListener {
	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		setContentView(R.layout.tips);
		findViewById(R.id.touch_to_start).setOnClickListener(this);
	}

	@Override
	public void onClick(View view) {
		if (view.getId() == R.id.touch_to_start) {
			Intent i = new Intent(this, AgileBuddyActivity.class);
			startActivity(i);
			this.finish();
		}
	}
}
