package reone.android.mandown.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import java.util.ArrayList;

import reone.android.mandown.Activity.sup.BaseActivity;
import reone.android.mandown.R;
import reone.android.mandown.util.AppTools;
import reone.android.mandown.util.ConstantInfo;

public class SplashActivity extends BaseActivity implements OnClickListener {

    private SharedPreferences mBaseSettings;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        initUtil();
        setContentView(R.layout.splash);

        Button startButton = (Button) findViewById(R.id.start_game);
        startButton.setOnClickListener(this);

        Button scoreBoardButton = (Button) findViewById(R.id.score_board);
        scoreBoardButton.setOnClickListener(this);

        Button goProButton = (Button) findViewById(R.id.more_app);
        goProButton.setOnClickListener(this);

        Button optionButton = (Button) findViewById(R.id.options);
        optionButton.setOnClickListener(this);

        Button exitButton = (Button) findViewById(R.id.exit);
        exitButton.setOnClickListener(this);

        initBGselect();
        mBaseSettings = PreferenceManager.getDefaultSharedPreferences(this);
    }

    private void initBGselect() {
        View bg1 = findViewById(R.id.bg_select_1);
        bg1.setOnClickListener(this);
        View bg2 = findViewById(R.id.bg_select_2);
        bg2.setOnClickListener(this);
        View bg3 = findViewById(R.id.bg_select_3);
        bg3.setOnClickListener(this);
        View bg4 = findViewById(R.id.bg_select_4);
        bg4.setOnClickListener(this);
    }

    private void sendClickToBgselect(View v) {
        int backgroudRes = -1;
        switch (v.getId()){
            case R.id.bg_select_1:
                backgroudRes = R.drawable.bg_game;
                break;
            case R.id.bg_select_2:
                backgroudRes = R.drawable.bg_game2;
                break;
            case R.id.bg_select_3:
                backgroudRes = R.drawable.bg_game3;
                break;
            case R.id.bg_select_4:
                backgroudRes = R.drawable.bg_game4;
                break;
        }
        if(backgroudRes!=-1){
            findViewById(R.id.splash_parent).setBackground(getResources().getDrawable(backgroudRes));
            AppTools.getSharedPreferencesEdit().putInt(ConstantInfo.PREFERENCE_KEY_BACKGROUD_RES,backgroudRes).commit();
        }
    }

    private void initUtil() {
        AppTools.mContext = mContext.getApplicationContext();
    }

    public void onClick(View v) {
        Intent i = null;
        switch (v.getId()) {
            case R.id.start_game:
                if (mBaseSettings.getBoolean(ConstantInfo.PREFERENCE_KEY_SHOWTIPS,
                        true)) {
                    i = new Intent(this, TipsActivity.class);
                } else {
                    i = new Intent(this, AgileBuddyActivity.class);
                }
                break;
            case R.id.options:
                i = new Intent(this, PrefsActivity.class);
                break;
            case R.id.score_board:
                i = new Intent(this, GlobalRankingActivity.class);
                break;
            case R.id.more_app:
                i = new Intent(Intent.ACTION_VIEW, Uri
                        .parse("market://search?q=益智游戏"));
                break;
            case R.id.exit:
                finish();
                return;
            default:
                sendClickToBgselect(v);
        }
        if (i != null) {
            startActivity(i);
        }
    }

}
